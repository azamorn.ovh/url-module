import { Request, Response, Router, json } from 'express';
import { AzamornOVH } from '../../src/main';
import { Module } from '../module';
import dotenv = require('dotenv');
import Joi = require('@hapi/joi');

export class ShortURLModule extends Module{
    config: any;
    shorturl_schema : any;
    app: AzamornOVH; 
    constructor (app: AzamornOVH) {
        super("ShortURL");
        this.app = app;
        this.config = dotenv.config().parsed;
        this.shorturl_schema = Joi.object({
            url: Joi.string().uri().required()
        });
        return this;
    }
    init () : Promise <void> {
        return new Promise((resolve) => {
            const shorturls = this.app.db.get('shorturls');
            const router = Router();
            router.get('/:shortcode', async (req: Request, res: Response) => {
                let { shortcode } = req.params;
                shorturls.findOne({code: shortcode}).then((doc) => {
                    res.redirect(doc.url)
                }).catch((err) => {
                    res.sendStatus(404);
                })
            });
            router.post('/', json(), async (req: Request, res: Response) => {
                this.shorturl_schema.validateAsync(req.body).then((data) => {
                    let shortcode = this.unique_id();
                    shorturls.insert({ url: data.url, code: shortcode})
                    res.status(200).json({shortcode : `${this.app.config.API_PATH}url/${shortcode}`})
                }).catch((err) => {
                    res.status(200).json({err: err});
                });
            });
            router.all('*', (req: Request, res: Response) => {
                res.sendStatus(401);
            });
            this.app.api.use('/url', router)
            resolve();
        })
    }
    unique_id(){ 
        return Math.random().toString(36).substring(2, 5) + Math.random().toString(36).substring(2, 5)
    }
}